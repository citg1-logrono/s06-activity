-- List books Authored by Marjorie Green
        - "The Busy Executives Database Guide"
        - "You Can Combat Computer Stress"
    -- List the books Authored by Michael O'Leary
        - "Cooking with Computers"
    -- Write the author/s of "The Busy Executives Database Guide"
        - "Marjorie Green"
        - "Abraham Bennet"
    -- Identify the publisher of "But is it User Friendly"
        -"Algodata Infosystem"
    -- List the books published by Algodata Infosystem
        - "The Busy Executives Database Guide"
        - "Cooking with Computers"
        - "Straight Talk About Computers"
        - "But is it User Friendly"
        - "Secrets of Silicon Valley"
        - "Net Etiquette"


Microsoft Windows [Version 10.0.19044.2604]
(c) Microsoft Corporation. All rights reserved.

C:\Users\acer>mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 289
Server version: 10.4.24-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+----------------------+
| Database             |
+----------------------+
| db_logrono_sims      |
| db_technovento_harvy |
| dbclinicsystem       |
| dbdentalclinicsystem |
| dbeventsystem        |
| information_schema   |
| music_store          |
| mysql                |
| performance_schema   |
| phpmyadmin           |
| restaurant           |
| test                 |
+----------------------+
12 rows in set (0.001 sec)

MariaDB [(none)]> CREATE DATABASE blog_db;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> USE blog_db;
Database changed
MariaDB [blog_db]> CREATE TABLE users(
    -> id INT NOT NULL AUTO_INCREMENT,
    -> email VARCHAR(100) NOT NULL,
    -> password VARCHAR(300) NOT NULL,
    -> datetime_create DATETIME NOT NULL,
    -> PRIMARY KEY(id)
    -> );
Query OK, 0 rows affected (0.048 sec)

MariaDB [blog_db]> CREATE TABLE posts(
    -> id INT NOT NULL AUTO_INCREMENT,
    -> author_id INT NOT NULL,
    -> title VARCHAR(500) NOT NULL,
    -> content VARCHAR(5000) NOT NULL,
    -> datetime_posted DATETIME NOT NULL,
    -> PRIMARY KEY(id),
    -> CONSTRAINT fk_author_id
    ->     FOREIGN KEY(author_id) REFERENCES users(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.105 sec)

MariaDB [blog_db]> CREATE TABLE post_comments(
    -> id INT NOT NULL AUTO_INCREMENT,
    -> post_id INT NOT NULL,
    -> user_id INT NOT NULL,
    -> datetime_commented DATETIME NOT NULL,
    -> PRIMARY KEY(id),
    -> CONSTRAINT fk_post_id
    ->     FOREIGN KEY(post_id) REFERENCES posts(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT,
    -> CONSTRAINT fk_user_id
    ->     FOREIGN KEY(user_id) REFERENCES users(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.080 sec)

MariaDB [blog_db]> CREATE TABLE post_likes(
    -> id INT NOT NULL AUTO_INCREMENT,
    -> post_id INT NOT NULL,
    -> user_id INT NOT NULL,
    -> datetime_liked DATETIME NOT NULL,
    -> PRIMARY KEY(id),
    -> CONSTRAINT fk_post_id2
    ->     FOREIGN KEY(post_id) REFERENCES posts(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT,
    -> CONSTRAINT fk_user_id2
    ->     FOREIGN KEY(user_id) REFERENCES users(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.078 sec)